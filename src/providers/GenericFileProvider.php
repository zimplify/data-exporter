<?php
    namespace Zimplify\Exporter\Providers;
    use Zimplify\Core\{Application, File, Provider};
    use Zimplify\Core\{StorageProvider};
    use \RuntimeException;
    
    /**
     * <provide description for this service provider>
     * @package Zimplify\Exporter (code 11)
     * @type Provider (code 03)
     * @file GenericFileProvider (code 01)
     */
    abstract class GenericFileProvider extends Provider {
    
        const ARGS_REMOTE_STORE = "remote";
        const ARGS_STORAGE_BUCKET = "bucket";
        const CFG_REMOTE_BUCKET = "system.providers.reporter.bucket";
        const DEF_CLS_NAME = "Zimplify\Exporter\Providers\GenericFileProvider";
        const DEF_FILE_SIZE = 3;
        const DEF_SHT_NAME = "core-export::generic-file";
        const DRV_ARG_SIZE = "file-size";
        const ENV_STORE_PATH = "system.paths.report-store";
        const ERR_NO_BUCKET = 500110301001;

        /**
         * the main routine that all clients will trigger
         * @param array $dataset the dastaset offered by the client for formatting
         * @param string $name (optional) predetermined name to use for writing
         * @return string
         */
        protected function encode(array $dataset, string $name = null) : string {
            $destination = $name ?? $this->name();

            // write our header first
            file_put_contents($destination, $this->header($dataset), FILE_APPEND);
            foreach ($dataset as $entry) {
                $data = $this->format($entry);
                $this->debug("ENTRY: ".json_encode($entry), __FUNCTION__);
                file_put_contents($destination, $data, FILE_APPEND);
            }
            
            return $destination;
        }        

        /**
         * get the extension of the file to export
         * @return string
         */
        protected abstract function extension() : string;
    
        /**
         * initializing the provider.
         * @return void
         */
        protected function initialize() {
        }
    
        /**
         * check if all required arguments are supplied into the provider for initialization.
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * formatting the data into the format that user will be able to use
         * @param array $entry EACH entry of the key data to use
         * @return string
         */
        protected abstract function format(array $entry) : string;

        /**
         * get the expected MIME type for return
         * @return string
         */
        protected abstract function getFileType() : string;

        
        /**
         * prepare the file header for the report file
         * @param array $dataset the main dataset we are receiving
         * @return string
         */
        protected function header(array $dataset) : string {
            return "";            
        }        

        /**
         * create a temporary name and storage of the report
         * @return string
         */
        protected function name() : string {
            $size = array_key_exists(self::DRV_ARG_SIZE, $this->driver()) ? (int)$this->driver()[self::DRV_ARG_SIZE] : self::DEF_FILE_SIZE;
            $directory = Application::env(self::ENV_STORE_PATH);
            $name = "";
            for ($i = 0; $i < $size; $i++) 
                $name .= uniqid("", true);
            return "$directory/$name.report".$this->extension();
        }

        /**
         * the main method to trigger the writing of the report
         * @param array $dataset the data source for us to write
         * @return File
         */
        public function run(array $dataset) : File {
            $stored = $this->encode($dataset);

            // now run the file create
            $result = new File();
            $result->location = $stored;
            $result->mime = $this->getFileType();
            $result->size = file_exists($stored) ? filesize($stored) : 0;

            // check we are output to storage provider
            if ($this->get(self::ARGS_REMOTE_STORE) === true) {
                $adapter = Application::request(StorageProvider::DEF_SHT_NAME, []);
                $bucket = Application::env(self::CFG_REMOTE_BUCKET);
                if (!is_null($bucket)) {                
                    $adapter->store($result, [self::ARGS_STORAGE_BUCKET => $bucket]);
                } else 
                    throw new RuntimeException("Unable to store to remote destination without defined bucket", self::ERR_NO_BUCKET);                
            }

            return $result;
        }
    }