<?php
    namespace Zimplify\Exporter\Providers;
    use Zimplify\Exporter\Providers\GenericFileProvider;
    use \RuntimeException;
    
    /**
     * the file convertor supports JSON file exports
     * @package Zimplify\Exporter (code 11)
     * @type Provider (code 03)
     * @file JsonFileProvider (code 03)
     */
    class JsonFileProvider extends GenericFileProvider {
    
        const DEF_CLS_NAME = "Zimplify\Exporter\Providers\JsonFileProvider";
        const DEF_FILE_TYPE = ".json";
        const DEF_MIME_TYPE = "application/json";
        const DEF_SEPERATOR = ",";
        const DEF_SHT_NAME = "core-export::json-file";

        /**
         * the main routine that all clients will trigger
         * @param array $dataset the dastaset offered by the client for formatting
         * @param string $name (optional) predetermined name to use for writing
         * @return File
         */
        protected function encode(array $dataset, string $name = null) : string {
            $destination = $name ?? $this->name();

            // write our header first
            file_put_contents($destination, json_encode($dataset));

            // now return the destination
            return $destination;
        }                

        /**
         * get the extension of the file to export
         * @return string
         */
        protected function extension() : string {
            return self::DEF_FILE_TYPE;
        }        
    
        /**
         * formatting the data into the format that user will be able to use
         * @param array $entry EACH entry of the key data to use
         * @return string
         */
        protected function format(array $entry) : string {
            return json_encode($entry);
        }

        /**
         * get the expected MIME type for return
         * @return string
         */
        protected function getFileType() : string {
            return self::DEF_MIME_TYPE;
        }


    }    