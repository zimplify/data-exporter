<?php
    namespace Zimplify\Exporter\Providers;
    use Zimplify\Exporter\Providers\GenericFileProvider;
    use \RuntimeException;
    
    /**
     * the file convertor supports CSV file exports
     * @package Zimplify\Exporter (code 11)
     * @type Provider (code 03)
     * @file CsvFileProvider (code 02)
     */
    class CsvFileProvider extends GenericFileProvider {
    
        const DEF_CLS_NAME = "Zimplify\Exporter\Providers\CsvFileProvider";
        const DEF_FILE_TYPE = ".csv";
        const DEF_MIME_TYPE = "text/csv";
        const DEF_SEPERATOR = ",";
        const DEF_SHT_NAME = "core-export::csv-file";

        /**
         * get the extension of the file to export
         * @return string
         */
        protected function extension() : string {
            return self::DEF_FILE_TYPE;
        }        
    
        /**
         * formatting the data into the format that user will be able to use
         * @param array $entry EACH entry of the key data to use
         * @return string
         */
        protected function format(array $entry) : string {
            $result = [];
            foreach ($entry as $field => $value) 
                array_push($result, $value);            
            return implode(self::DEF_SEPERATOR, $result)."\n";
        }

        /**
         * prepare the file header for the report file
         * @param array $dataset the main dataset we are receiving
         * @return string
         */
        protected function header(array $dataset) : string {
            $this->debug("\$dataset: ".json_encode($dataset), __FUNCTION__);
            $result = [];
            foreach ($dataset[0] as $field => $value) 
                array_push($result, $field);
            return implode(self::DEF_SEPERATOR, $result)."/n";
        }                

        /**
         * get the expected MIME type for return
         * @return string
         */
        protected function getFileType() : string {
            return self::DEF_MIME_TYPE;
        }
    }    